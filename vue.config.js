// vue.config.js
module.exports = {
  // ref https://medium.com/@mrodal/how-to-make-lazy-loading-actually-work-in-vue-cli-3-7f3f88cfb102
  chainWebpack: (config) => {
    config.plugins.delete('prefetch')
  },
  // refs
  // https://cli.vuejs.org/guide/webpack.html#simple-configuration
  // https://medium.com/webpack/webpack-4-code-splitting-chunk-graph-and-the-splitchunks-optimization-be739a861366
  // https://webpack.js.org/plugins/split-chunks-plugin/
  configureWebpack: {
    optimization: {
      splitChunks: {
        chunks: 'all'
      }
    }
  }
}
