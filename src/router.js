import Vue from 'vue'
import Router from 'vue-router'
// Configurator module global routes
import ConfiguratorRoutes from '@/modules/Configurator/Routes'
import CoreRoutes from '@/core/Routes'

Vue.use(Router)

var allRoutes = []
allRoutes = allRoutes.concat(CoreRoutes, ConfiguratorRoutes)

export default new Router({
  routes: allRoutes
})
