import { http } from '@/http'

export default {
  getConfiguratorData: function () {
    return http
      .get('preferences/1')
  }
}
