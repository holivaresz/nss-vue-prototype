/* Lazy load chunks for routes in the same module. I think
all module routes should be use the same webpackChunkName, in the example below
we use the webpackChunkName=configurator for all the routes in the configurator module

route level code-splitting
this generates a separate chunk (configurator.[hash].js) for this route
which is lazy-loaded when the route is visited.

ref: https://router.vuejs.org/guide/advanced/lazy-loading.html#grouping-components-in-the-same-chunk
*/
const routes = [
  {
    path: '/configurator',
    name: 'configurator',
    component: () => import(/* webpackChunkName: "configurator" */ '@/modules/Configurator/Configurator.vue'),
    meta: { keepAlive: true }, // Inactive components will be cached
    children: [
      {
        path: 'home',
        name: 'conf_home',
        component: () => import(/* webpackChunkName: "configurator" */ '@/modules/Configurator/Pages/Home.vue')
      },
      {
        path: 'about',
        name: 'conf_about',
        component: () => import(/* webpackChunkName: "configurator" */ '@/modules/Configurator/Pages/About.vue')
      },
      {
        path: 'posts',
        name: 'conf_post',
        meta: { keepAlive: true }, // Inactive components will be cached
        component: () => import(/* webpackChunkName: "configurator" */ '@/modules/Configurator/Pages/Post.vue')
      },
      {
        path: 'preferences/:pref',
        name: 'conf_pref',
        meta: { keepAlive: true }, // Inactive components will be cached
        component: () => import(/* webpackChunkName: "configurator" */ '@/modules/Configurator/Pages/Preference.vue')
      }
    ]
  }
]

export default routes
