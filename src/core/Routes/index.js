const routes = [
  {
    path: '/',
    name: 'home',
    meta: { keepAlive: true }, // Inactive components will be cached
    component: () => import(/* webpackChunkName: "core" */ '@/core/Pages/Home.vue')
  }
]

export default routes
